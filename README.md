# Mind Discord Bot

https://mind-discord-bot-dot-mind-5.appspot.com

## Development

### Run local

```bash
npm install
```

```bash
npm start
```

### Deploy to GoogleCloudEngine

```bash
gcloud app deploy
```
