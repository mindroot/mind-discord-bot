const { Client, RichEmbed } = require('discord.js');
const client = new Client();
const axios = require('axios');
const { generateKeyPair, generateUuid, is_validURL } = require('./lib/tools');

const { composeAPI } = require('@iota/core');
const validators = require('@iota/validators');

const Mam = require('./lib/mam.client.js')
const { asciiToTrytes, trytesToAscii } = require('@iota/converter')

const iota = composeAPI({
    provider: 'https://nodes.devnet.thetangle.org:443'
})

// Initialise MAM State
let mamState = Mam.init('https://nodes.devnet.thetangle.org:443')


require('dotenv').config() // Loads .env

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', message => {

  if (message.author.bot) return;

  if (message.content === '!ping') {
    message.reply('Pong!');
  }
  if (message.content === '!help') {
    // We can create embeds using the MessageEmbed constructor
    // Read more about all that you can do with the constructor
    // over at https://discord.js.org/#/docs/main/stable/class/RichEmbed
    const embed = new RichEmbed()
      // Set the title of the field
      .setTitle('Mind Bot Help')
      // Set the color of the embed
      .setColor(0x70C8B6)
      // Set the main content of the embed
      .setDescription(`
        !balance IOTAADDRESS
        !info mind
        !info events
        !info quests
        !next quest|event
        !today event
        `
      );
    // Send the embed to the same channel as the message
    message.reply(embed);
  }

  if (message.content === '!nodeinfo') {
    message.reply('Ping!');
    iota.getNodeInfo()
      .then(info => message.reply("Pong: " + JSON.stringify(info)))
      .catch(err => {})
  }

  if (message.content.includes('!balance')) {
    const data = message.toString().split(' ')
    const address = data[1]

    iota.getBalances([address], 100)
      .then(({ balances }) => {
        balances.forEach((balance) => {

          const embed = new RichEmbed()
            // Set the title of the field
            .setTitle('Balance: ' + balance)
            // Set the color of the embed
            .setColor(0x70C8B6)
            // Set the main content of the embed
            .setDescription('https://devnet.thetangle.org/address/' + address);
          message.reply(embed);
        })
      })
      .catch(err => {})
    }

  if (message.content === '!info world of mind' || message.content === '!info mind' || message.content === '!info m') {
    // Send the embed to the same channel as the message
    const embed = new RichEmbed()
      // Set the title of the field
      .setTitle('Mind - World of Mind')
      // Set the color of the embed
      .setColor(0x70C8B6)
      // Set the main content of the embed
      .setDescription(`
        A distributed community-driven social network.\n
        https://ecosystem.iota.org/projects/mind`);
    message.reply(embed);
  }

  if (message.content === '!info events' || message.content === '!info event' || message.content === '!info e') {
    // Send the embed to the same channel as the message
    const embed = new RichEmbed()
      // Set the title of the field
      .setTitle('Mind Events')
      // Set the color of the embed
      .setColor(0x70C8B6)
      // Set the main content of the embed
      .setDescription(`
        Events is the world first platform to create Events on IOTA's Tangle. Meet the community from all around the globe.\n
        https://events.worldofmind.org`);
    message.reply(embed);
  }

  if (message.content === '!next event' || message.content === '!next e') {
    // Send the embed to the same channel as the message
    axios.get('https://events.worldofmind.org/next_event.json')
        .then(response => {
            console.log("response!", response);
            const event = response.data
            var start_on = new Date(event.start_on);
            var options = { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric', hour: '2-digit', minute: '2-digit' };

            const embed = new RichEmbed()
              // Set the title of the field
              .setTitle('Next Event: ' + event.title + " on " + start_on.toLocaleDateString("en-US", options))
              // Set the color of the embed
              .setColor(0x70C8B6)
              // Set the main content of the embed
              .setDescription(event.link);
            message.reply(embed);
          })
  }

  if (message.content === '!today event' || message.content === '!today events'|| message.content === '!today e') {
    // Send the embed to the same channel as the message
    const today = new Date().toJSON().slice(0,10).replace(/-/g,'-');
    console.log("today", today);
    axios.get('https://events.worldofmind.org/get_all_events_for_date.json?date=' + today)
        .then(response => {
            const events = response.data;
            console.log("events", events);
            let titles = events.map((event) => console.log("event", event) )
            console.log("titles", titles);

            const embed = new RichEmbed()
              // Set the title of the field
              .setTitle('Events Today: ' + events.length)
              // Set the color of the embed
              .setColor(0x70C8B6)
              // Set the main content of the embed
              .setDescription(`${events.map((event) => `\n${event.title}: ${event.link}`)}` );
            message.reply(embed);
          })
  }

  if (message.content === '!next quest' || message.content === '!next q') {
    // Send the embed to the same channel as the message
    axios.get('https://quests.worldofmind.org/next_quest.json')
        .then(response => {
            console.log("response!", response);
            const quest = response.data

            const embed = new RichEmbed()
              // Set the title of the field
              .setTitle('Next Quest: ' + quest.title)
              // Set the color of the embed
              .setColor(0x70C8B6)
              // Set the main content of the embed
              .setDescription(`
                Balance: ${quest.balance ? quest.balance : '0'}
                Link: ${quest.link}`);
            message.reply(embed);
          })
  }

  if (message.content === '!info quests' || message.content === '!info quest' || message.content === '!info q') {
    // Send the embed to the same channel as the message
    const embed = new RichEmbed()
      // Set the title of the field
      .setTitle('Mind Quests')
      // Set the color of the embed
      .setColor(0x70C8B6)
      // Set the main content of the embed
      .setDescription(`
        Quests is the worlds first platform where you can earn IOTA for solving issues or incentivize issues to get them solved by others.\n
        https://quests.worldofmind.org`);
    message.reply(embed);
  }

  if (message.content === '!create account' || message.content === '!c a') {

    const embed = new RichEmbed()
      // Set the title of the field
      .setTitle('Your Mind Account will be created!')
      // Set the color of the embed
      .setColor(0x70C8B6)
      // Set the main content of the embed
      .setDescription(`
        That will takes some seconds...
        `);
    message.reply(embed);


    const uuid = generateUuid();
    const keyPairs = generateKeyPair();

    const values = {
      first_name: message.author.tag,
      last_name: 'Test',
      cosignerp: '',
      cosigners: '',
      profile_picture: message.author.avatar_url,
    }

    const params = Object.assign({ pk: keyPairs.pk }, values);

    createIdentity(uuid, params)
      .then((txHash) => {
        console.log('Identity created:', txHash);
        // Send the embed to the same channel as the message
        const embed2 = new RichEmbed()
          // Set the title of the field
          .setTitle('Mind Account successfully created!')
          // Set the color of the embed
          .setColor(0x70C8B6)
          // Set the main content of the embed
          .setDescription(`
            You can watch your transaction here: \n
            https://devnet.thetangle.org/transaction/${txHash}\n
            `);
        message.reply(embed2);

        const embed3 = new RichEmbed()
          // Set the title of the field
          .setTitle('Mind Account successfully created!')
          // Set the color of the embed
          .setColor(0x70C8B6)
          // Set the main content of the embed
          .setDescription(`
            Your mind uuid: ${uuid} \n
            Your mind public key: \n
            ${keyPairs.pk}\n
            `);
        message.reply(embed3);
        const embed4 = new RichEmbed()
          // Set the title of the field
          .setTitle('Your Private key - DONT SHARE IT')
          // Set the color of the embed
          .setColor(0x70C8B6)
          // Set the main content of the embed
          .setDescription(`${keyPairs.sk}`);
        message.author.send(embed4);
      });

  }

  if (message.content === '!login help') {


    // Send the embed to the same channel as the message
    const embed = new RichEmbed()
      // Set the title of the field
      .setTitle('Mind TangleID Login')
      // Set the color of the embed
      .setColor(0x70C8B6)
      // Set the main content of the embed
      .setDescription(`
        \n
        Combine your Discord Account with our Mind TangleID.\n
        Send your credentials to the Mind Bot, to log in.\n
        !private_key ???\n
        `);
}


  const config = {
    prefix: '!'
  }

console.log("show_config.prefix", config.prefix);

const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
const command = args.shift().toLowerCase();

if (message.content.indexOf(config.prefix) !== 0) return;


if (command === 'mam') {
  let mam_command = args[0];
  let root = args[1];

  if(mam_command === undefined ) {
      message.reply("Add an mam command!");
    return
  };

  if(root === undefined ) {
      message.reply("Add an mam root behind the command!");
    return
  };

  if(validators.isHash(`${root}`)) {

    if (mam_command == "fetch" ) {
      const embed = new RichEmbed()
        // Set the title of the field
        .setTitle('MAM Channel Start')
        // Set the color of the embed
        .setColor(0x70C8B6)
        // Set the main content of the embed
        .setDescription(`
          root: ${root}
          `);

      message.reply(embed);
      execute(root, message).then(data => {
        // Send the embed to the same channel as the message
        const embed = new RichEmbed()
          // Set the title of the field
          .setTitle('MAM Channel End')
          // Set the color of the embed
          .setColor(0x70C8B6)
          // Set the main content of the embed
          .setDescription(`
            next root: ${data.nextRoot}
            `);

        message.reply(embed);
      })
    }
  } else {
    message.reply("Add an correct mam root!");
    return
  }
}



  console.log("args", args);
  console.log("command", command);

  if(command === 'show') {
    let url = args[0];
    if(url === undefined ) {
        message.reply("Add an url behind the command!");
      return
    };
    if(is_validURL(url)) {
      axios.get(url + '.json')
          .then(response => {
              console.log("response!", response);
              const data = response.data
                const embed = new RichEmbed()
                // Set the title of the field
                .setTitle(data.title)
                // Set the color of the embed
                .setColor(0x70C8B6)
                // Set the main content of the embed
                .setDescription(`
                    more data soon...\n
                  `);
              message.reply(embed);
            })
    } else {
      message.reply("Please enter a valid url!");
    }
  }



});

// Publish to tangle
const publish = async packet => {
    const trytes = asciiToTrytes(JSON.stringify(packet))
    const message = Mam.create(mamState, trytes)
    mamState = message.state
    console.log("1");
    await Mam.attach(message.payload, message.address)
    console.log("2");

    return message.root
}


const execute = async (root, discord_message) => {
    // Publish and save root.
    // "HYDEFLA9PGSBVBJCJAMIXKLPMNUVJHTQBPUEZ9VBXTLRSHUYZPSIJGEEGSOBBBHCXJDKUN9RQTMPDBGTJ"
    // Callback used to pass data + returns next_root
    const resp = await Mam.fetch(root, 'public', null, (entry) => {
        discord_message.reply(trytesToAscii(entry));
        console.log(JSON.parse(trytesToAscii(entry)))
    })

    console.log(resp, root)
    console.log("resp", resp)
    console.log("root", root)
    return {
      nextRoot: resp.nextRoot
    }
}

const createIdentity = function(uuid, input) {
  const claim = Object.assign({
    command: 'new_user',
    uuid,
    first_name: '',
    last_name: '',
    cosignerp: '',
    cosigners: '',
    profile_picture: '',
    pk: '',
  }, input);

  // forward request to Backend API
  return fetch(`http://node2.puyuma.org:8000`, {
    method: 'POST',
    body: JSON.stringify(claim),
    headers: {
      'Content-Type': 'application/json; charset=utf-8',
    },
  })
    .then(response => response.text())
    .catch((error) => {
      console.error(error);
    });
}

client.login(process.env.DISCOD_BOT_KEY);
