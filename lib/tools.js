const forge = require('node-forge');
const crypto = require('crypto');
const { asciiToTrytes, trytesToAscii } = require('@iota/converter')

const d64 = data => forge.util.decode64(data);
const e64 = data => forge.util.encode64(data);
const dUTF = data => forge.util.decodeUtf8(data);
const eUTF = data => forge.util.encodeUtf8(data);

const generateKeyPair = () => {
  const keypair = forge.pki.rsa.generateKeyPair({ bits: 2048, e: 0x10001 });
  return {
    pk: forge.pki.publicKeyToPem(keypair.publicKey),
    sk: forge.pki.privateKeyToPem(keypair.privateKey),
  };
};

const seedGen = (length) => {
  const charset = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ9';
  let result = '';
  const buf = crypto.randomBytes(4 * length);
  for (let i = 0; i < length; ++i) {
    result += charset.charAt(buf.readUInt32LE(i) % charset.length);
  }
  return result;
};

const generateUuid = () => seedGen(26);

const sign = (msg, privateKeyPem) => {
  const privateKey = forge.pki.privateKeyFromPem(privateKeyPem);
  const md = forge.md.sha256.create();
  md.update(msg);
  return e64(privateKey.sign(md));
};

const verify = (msg, signature, publicKeyPem) => {
  const publicKey = forge.pki.publicKeyFromPem(publicKeyPem);
  const md = forge.md.sha256.create();
  md.update(msg);
  return publicKey.verify(md.digest().bytes(), d64(signature));
};

// Encrypt data for transmit
const encrypt = (msg, publicKeyPem) => {
  const publicKey = forge.pki.publicKeyFromPem(publicKeyPem);
  return e64(publicKey.encrypt(msg));
};

// Decrypt data
const decryptUTF = (msg, privateKeyPem) => {
  const privateKey = forge.pki.privateKeyFromPem(privateKeyPem);
  return eUTF(privateKey.decrypt(d64(msg)));
};

const decrypt64 = (msg, privateKeyPem) => {
  const privateKey = forge.pki.privateKeyFromPem(privateKeyPem);
  return e64(privateKey.decrypt(d64(msg)));
};


// Create
const generatePacket = (issuerID, msg, sig, receiverID) => ({
  claim: msg,
  signature: sig,
  issuer: issuerID,
  id: receiverID,
});

// Create
const generateInitialPacket = (uuid, msg, sig, pk) => ({
  claim: msg,
  signature: sig,
  pk,
  id: uuid,
});

const is_validURL = (str) => {
  var pattern = new RegExp('^(https?:\\/\\/)?'+ // protocol
    '((([a-z\\d]([a-z\\d-]*[a-z\\d])*)\\.?)+[a-z]{2,}|'+ // domain name
    '((\\d{1,3}\\.){3}\\d{1,3}))'+ // OR ip (v4) address
    '(\\:\\d+)?(\\/[-a-z\\d%_.~+]*)*'+ // port and path
    '(\\?[;&a-z\\d%_.~+=-]*)?'+ // query string
    '(\\#[-a-z\\d_]*)?$','i'); // fragment locator
  if(!pattern.test(str)) {
    return false;
  } else {
    return true;
  }
};

// Callback used to pass data out of the fetch
const logData = data => console.log(JSON.parse(trytesToAscii(data)))


// Publish to tangle
const publish = async packet => {
    const trytes = asciiToTrytes(JSON.stringify(packet))
    const message = Mam.create(mamState, trytes)
    mamState = message.state
    await Mam.attach(message.payload, message.address)
    return message.root
}

module.exports = {
  d64,
  e64,
  dUTF,
  eUTF,
  generateKeyPair,
  seedGen,
  generateUuid,
  sign,
  verify,
  encrypt,
  decrypt64,
  decryptUTF,
  generatePacket,
  generateInitialPacket,
  is_validURL,
  logData,
  publish
};
